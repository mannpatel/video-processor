import '@tensorflow/tfjs-backend-webgl';
const bodyPix = require('@tensorflow-models/body-pix');
class BlurVideoProcessor {
  outputFrame: any;
  constructor() {
    this.outputFrame = new OffscreenCanvas(0, 0);
  }
  async processFrame(inputFrame: { width: any; height: any; transferToImageBitmap: () => any }) {
    this.outputFrame.width = inputFrame.width;
    this.outputFrame.height = inputFrame.height;

    const net = await bodyPix.load();
    const segmentation = await net.segmentPerson(inputFrame);

    const backgroundBlurAmount = 8;
    const edgeBlurAmount = 3;
    const flipHorizontal = false;

    bodyPix.drawBokehEffect(
      this.outputFrame,
      inputFrame,
      segmentation,
      backgroundBlurAmount,
      edgeBlurAmount,
      flipHorizontal
    );

    return this.outputFrame;
  }
}

export const blurVideoProcessor = new BlurVideoProcessor();
