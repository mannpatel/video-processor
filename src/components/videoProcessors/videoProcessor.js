import { createLocalVideoTrack, VideoProcessor } from 'twilio-video';
import { GaussianBlurBackgroundProcessor, VirtualBackgroundProcessor } from '@twilio/video-processors';

export default function videoProcessor() {
  const blurBackground = new GaussianBlurBackgroundProcessor({
    assetsPath: '../',
  });
  const a = blurBackground.loadModel().then(() => {
    createLocalVideoTrack({
      width: 640,
      height: 480,
      frameRate: 24,
    }).then(track => {
      track.addProcessor(blurBackground);
    });
  });
  let name = 'mann';
  return;
}
