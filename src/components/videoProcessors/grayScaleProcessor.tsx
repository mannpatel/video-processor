import React from 'react';
import Video from 'twilio-video';
class GrayScaleProcessor {
  percentage: any;
  constructor(percentage: any) {
    this.percentage = percentage;
  }
  processFrame(inputFrameBuffer: any, outputFrameBuffer: any) {
    const context = outputFrameBuffer.getContext('2d');
    context.filter = `grayscale(${this.percentage}%)`;
    context.drawImage(inputFrameBuffer, 0, 0, inputFrameBuffer.width, inputFrameBuffer.height);
    return this.percentage;
  }
}
export const grayScaleProcessor = new GrayScaleProcessor(100);
