import '@tensorflow/tfjs-backend-webgl';
// import Image from './waterfall.jpg';
const bodyPix = require('@tensorflow-models/body-pix');

class VirtualVideoProcessor {
  outputFrame: any;
  constructor() {
    this.outputFrame = new OffscreenCanvas(0, 0);
  }

  async processFrame(inputFrame: { width: any; height: any; transferToImageBitmap: () => any }) {
    this.outputFrame.width = inputFrame.width;
    this.outputFrame.height = inputFrame.height;

    // load bodyPix modal
    const net = await bodyPix.load();

    // create a segmetation of person
    const segmentation = await net.segmentPerson(inputFrame);

    // created a canvas to draw image
    const context = this.outputFrame.getContext('2d');
    context.drawImage(inputFrame, 0, 0, inputFrame.width, inputFrame.height);
    console.log('input frame', inputFrame);
    // getImagedata will provide image pixels in binary
    var imageData = context.getImageData(0, 0, inputFrame.width, inputFrame.height);

    // remove background by removing pixels
    let pixel = imageData.data;
    for (let p = 0; p < pixel.length; p += 4) {
      if (segmentation.data[p / 4] == 0) {
        pixel[p + 3] = 0;
      }
    }
    context.imageSmoothingEnabled = true;
    context.putImageData(imageData, 0, 0);
    // putting the only mask of person in canvas
    const contextPerson = this.outputFrame.getContext('2d');

    // var myImage = new Image(1280, 720);
    // myImage.src = './waterfall.jpg';
    // const imageBitmapPromise=createImageBitmap(myImage,0,0,32,32);

    // console.log("image",myImage);
    // contextPerson.drawImage(myImage,0,0,inputFrame.width,inputFrame.height);
    // var data = this.outputFrame.toDataURL("image/jpeg");
    //  context.putImageData(imageBitmapPromise, 0, 0);

    // const maskBackground = true;
    // // Convert the segmentation into a mask to darken the background.
    // const foregroundColor = { r: 0, g: 0, b: 0, a: 0 };
    // const backgroundColor = { r: 0, g: 0, b: 0, a: 555 };
    // const backgroundDarkeningMask = bodyPix.toMask(segmentation, foregroundColor, backgroundColor);

    // const opacity = 0.7;
    // const maskBlurAmount = 3;
    // const flipHorizontal = false;
    // // Draw the mask onto the image on a canvas.  With opacity set to 0.7 and
    // // maskBlurAmount set to 3, this will darken the background and blur the
    // // darkened background's edge.
    // bodyPix.drawMask(this.outputFrame, inputFrame, backgroundDarkeningMask, opacity, maskBlurAmount, flipHorizontal);
    return this.outputFrame;
  }
}

export const VirtualProcessor = new VirtualVideoProcessor();
