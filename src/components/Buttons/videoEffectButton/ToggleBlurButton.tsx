import React, { useState } from 'react';
import BlurOnIcon from '@material-ui/icons/BlurOn';
import BlurOffIcon from '@material-ui/icons/BlurOff';
import useLocalVideoToggle from '../../../hooks/useLocalVideoToggle/useLocalVideoToggle';
import useVideoContext from '../../../hooks/useVideoContext/useVideoContext';
import { LocalVideoTrack } from 'twilio-video';
import Button from '@material-ui/core/Button';
import videoProcessor from '../../videoProcessors/videoProcessor';
import { createLocalVideoTrack } from 'twilio-video';
import Video from 'twilio-video';
import { blurVideoProcessor } from '../../videoProcessors/videoProcessorSample';

import { grayScaleProcessor } from '../../videoProcessors/grayScaleProcessor';

{
  /* <script src="./twilio-video-processors.min.js"></script> */
}
//
// import twilio, { Twilio } from 'twilio';
// const { GaussianBlurBackgroundProcessor, VirtualBackgroundProcessor, isSupported } = Twilio.VideoProcessors;

export default function ToogleBlurButton() {
  //  console.log(Twilio)
  const [isVideoEnabled, toggleVideoEnabled] = useLocalVideoToggle();
  const [toggleBlurButton, setToggleBlurButton] = useState(false);
  console.log('success', process.env.MY_VARIABLE);
  // const setProcessor = (track: any, processor: any) => {
  //   track.addProcessor(processor);
  //   console.log('addedprocessor', track);
  // };
  console.log('dir path = ', __dirname, ' file path = ', __filename);

  async function gaussianBlurButton(event: any) {
    // event.preventDefault();
    // const gaussianBlurProcessor: any = new GaussianBlurBackgroundProcessor({
    //   assetsPath: '../',
    //   maskBlurRadius: 6,
    //   blurFilterRadius: 15,
    // });
    // await gaussianBlurProcessor.loadModel();
    // console.log('model loaded', gaussianBlurProcessor);
    // createLocalVideoTrack({
    //   width: 640,
    //   height: 480,
    //   frameRate: 24,
    // }).then(track => {
    //   track.addProcessor(gaussianBlurProcessor);
    // });
    // console.log('gaussian processor added', gaussianBlurProcessor);
  }

  //For My Custom implementation..
  // const { localTracks } = useVideoContext();
  // const videoTrack = localTracks.find(track => track.name.includes('camera')) as LocalVideoTrack;
  // React.useEffect(() => {
  //   if (videoTrack) {
  //     if (toggleBlurButton) {
  //       videoTrack.addProcessor(blurVideoProcessor);
  //       setToggleBlurButton(true);
  //     } else {
  //       videoTrack.removeProcessor(blurVideoProcessor);
  //       setToggleBlurButton(false);
  //     }
  //   }
  // }, [toggleBlurButton]);
  // const BlurVideo = () => {
  //   if (toggleBlurButton) {
  //     setToggleBlurButton(false);
  //   } else {
  //     setToggleBlurButton(true);
  //   }
  // };

  return (
    <Button
      id="gaussianBlur-Apply"
      onClick={gaussianBlurButton}
      disabled={!isVideoEnabled}
      startIcon={isVideoEnabled ? <BlurOnIcon /> : <BlurOffIcon />}
    >
      Blur Video
    </Button>
  );
}
