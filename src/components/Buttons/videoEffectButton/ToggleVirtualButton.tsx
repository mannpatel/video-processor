import React, { useState } from 'react';
import { Button } from '@material-ui/core';
import BlurOnIcon from '@material-ui/icons/BlurOn';
import BlurOffIcon from '@material-ui/icons/BlurOff';
import useLocalVideoToggle from '../../../hooks/useLocalVideoToggle/useLocalVideoToggle';
import useVideoContext from '../../../hooks/useVideoContext/useVideoContext';
import { LocalVideoTrack } from 'twilio-video';
import videoProcessor from '../../videoProcessors/videoProcessor';
import { createLocalVideoTrack, VideoProcessor } from 'twilio-video';
import Video from 'twilio-video';
import { VirtualProcessor } from '../../videoProcessors/videoprocessorSample1';
const ToggleVirtualButton = () => {
  const [isVideoEnabled, toggleVideoEnabled] = useLocalVideoToggle();
  const [toggleButton, setToggleButton] = useState(false);

  const { localTracks } = useVideoContext();
  const videoTrack = localTracks.find(track => track.name.includes('camera')) as LocalVideoTrack;
  React.useEffect(() => {
    if (videoTrack) {
      if (toggleButton) {
        videoTrack.addProcessor(VirtualProcessor);
        setToggleButton(true);
      } else {
        videoTrack.removeProcessor(VirtualProcessor);
        setToggleButton(false);
      }
    }
    // eslint-disable-next-line
  }, [toggleButton]);

  const VirtualVideo = () => {
    if (toggleButton) {
      setToggleButton(false);
    } else {
      setToggleButton(true);
    }
  };

  return (
    <>
      <Button
        onClick={VirtualVideo}
        disabled={!isVideoEnabled}
        startIcon={isVideoEnabled ? <BlurOnIcon /> : <BlurOffIcon />}
      >
        Virtual Video
      </Button>
    </>
  );
};

export default ToggleVirtualButton;
